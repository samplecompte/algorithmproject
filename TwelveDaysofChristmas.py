#Sam LeCompte
#Block 2
# 1/4/16

from Twelve_Days import *

xmas = TwelveDays()

Days = 12
Day = 1

print(xmas.getFirstLine(1))
print(xmas.getGift(1))

while Day < Days:
    Day += 1
    verse = xmas.getFirstLine(Day)
    n = Day
    while n > 1:
        verse += xmas.getGift(n)
        n -= 1
    verse += "And"
    verse += xmas.getGift(1)
    verse += "\n"
    print(verse)






