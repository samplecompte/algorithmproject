#Sam LeCompte
#Block 1
#1/5/16

n = int(input("Enter number of rows: "))
col = 1
row = 1
num = 1
triangle = ""

rowTotal = 0

while row <= n:
    while col <= row:
        triangle += str(num) + " "
        rowTotal += num
        num += 1
        col += 1
    triangle += " --> Sum: " + str(rowTotal) + " Formula: " + str(row*((row*row)+1)//2) + "\n"
    rowTotal = 0
    row += 1
    col = 1

print(triangle)
